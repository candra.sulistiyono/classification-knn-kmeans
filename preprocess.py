import re, string
import nltk
from nltk.corpus import stopwords
def preProcess(kalimat):
    lower_case = kalimat.lower()
    menghapus_angka = re.sub(r"\d+", "", lower_case)
    menghapus_tanda_baca = menghapus_angka.translate(str.maketrans("","",string.punctuation))
    menghapus_karakter_kosong = menghapus_tanda_baca.strip()
    tokens = menghapus_karakter_kosong.split()

    listStopword = set(stopwords.words('indonesian'))
    removed =  [word for word in tokens if word not in listStopword]

    from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    stemmed = [stemmer.stem(word) for word in removed]
    return stemmed
