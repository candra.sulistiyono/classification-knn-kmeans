from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cluster import KMeans
import re
import numpy as np
from collections import Counter
from preprocess import preProcess
from nltk.corpus import stopwords


true_test_labels = ['Sport', 'Kuliner', 'Properti']

print("Ada 7 dokumen di dalam masing masing kategori, berikut klasifikasi menggunakan K-NN dan klaster K-means dilakukan" \
" : \n1. "+true_test_labels[0]+" \n2. "+true_test_labels[1]+" \n3. "+true_test_labels[2]+"")
path = "BeritaDetail.txt"

train_clean_sentences = []
train_set = []
fp = open(path, 'r')
# Membaca semua isi berita di file
for line in fp:
    line = line.strip()
    cleaned = preProcess(line)
    cleaned = ' '.join(cleaned)
    train_clean_sentences.append(cleaned)


listStopword = set(stopwords.words('indonesian'))
vectorizer = TfidfVectorizer(stop_words=listStopword)
X = vectorizer.fit_transform(train_clean_sentences)

# Membuat label untuk 21 dokumen
y_train = np.zeros(21)
y_train[7:14] = 1
y_train[14:21] = 2

# Clustering dokumen KNN classifier dengan 3 kategori
modelknn = KNeighborsClassifier(n_neighbors=3)
modelknn.fit(X, y_train)

# Clustering dan training 31 dokumen dengan teknik K-means
modelkmeans = KMeans(n_clusters=3, init='k-means++', max_iter=200, n_init=100)
modelkmeans.fit(X)

test_judul = ["Ini Dia, 6 Stadion yang Dipilih FIFA Gelar Piala Dunia U-20 di Indonesia",
              "Cara Membuat dan Bumbu Pindang Bandeng yang Gurih Enak",
              "Waduh, Kuota Rumah Murah akan Habis April 2020?"]
test_judul2 = ["Stadion yang menjadi venue Piala Dunia U-20 2021 sudah dipilih FIFA. Berikut ini deretan stadion yang ditunjuk menggelar laga-laganya. Sebagai tindak lanjut penunjukkan Indonesia sebagai tuan rumah Piala Dunia U-20, rapat koordnasi lintas kementerian digelar, Selasa (22/1/2020). Bertempat di kantor Kementerian Pemuda dan Olahraga, Jalan Gerbang Pemuda, Senayan, rapat itu digelar. Sesmenpora, Gatot S Dewa Broto, yang memimpin rapat itu. hadir juga beberapa pejabat staf Kemenko PMK, Sekretarat Kabinet, Ditjen Kementerian Keuangan, hingga wakil dari PSSI.",
               "Ikan bandeng jadi salah satu sajian wajib saat imlek. Beragam olahan ikan bandeng dibuat untuk suguhan di hari istimewa. Inilah bumbu pindang bandeng yang enak. Ikan bandeng yang populer dengan sebutan 'milk fish' merupakan salah satu jenis ikan yang diternakkan di tambak. Dagingnya tebal dengan rasa gurih seperti susu, karenanya disebut 'milk fish'. Meskipun banyak duri, ikan ini tetap digemari banyak orang.",
               "Pengembang perumahan was-was menipisnya kuota rumah subsidi melalui Fasilitas Likuiditas Pembiayaan Properti (FLPP). Para pengembang memperkirakan fasilitas subsidi FLPP habis pada April mendatang. Pada 2020 mendatang, pemerintah mengalokasikan anggaran FLPP Rp 11 triliun untuk memfasilitasi 102.500 unit. Namun menurut Ketua Umum Real Estate Indonesia (REI) Totok Lusida saat ini hanya tersisa kuota 86 ribu unit. Angka tersebut diyakini akan habis April."]

print("\n Kalimat yang akan di lakukan test : \n")
test_clean_sentence = []
for test in test_judul:
    cleaned = preProcess(test)
    cleaned = ' '.join(cleaned)
    cleaned = re.sub(r"\d+", "", cleaned)
    test_clean_sentence.append(cleaned)


# Mulai mencari klasifikasinya
Test = vectorizer.transform(test_clean_sentence)
predicted_labels_knn = modelknn.predict(Test)
predicted_labels_kmeans = modelkmeans.predict(Test)

# Menampilkan ke layar
print("\n\nBelow 3 sentences will be predicted against the learned nieghbourhood and learned clusters:\n1. ", \
test_judul[0], "\n2. ", test_judul[1], "\n3. ", test_judul[2])
print("\n-------------------------------PREDICTIONS BY KNN------------------------------------------")
print("\n", test_judul[0], ":", true_test_labels[np.int(predicted_labels_knn[0])],
      "\n", test_judul[1], ":", true_test_labels[np.int(predicted_labels_knn[1])],
      "\n", test_judul[2], ":", true_test_labels[np.int(predicted_labels_knn[2])])

print("\n-------------------------------PREDICTIONS BY K-Means--------------------------------------")
print("\nIndex of "+true_test_labels[0]+" cluster : ", Counter(modelkmeans.labels_[0:6]).most_common(1)[0][0])
print("Index of "+true_test_labels[1]+" cluster : ", Counter(modelkmeans.labels_[7:13]).most_common(1)[0][0])
print("Index of "+true_test_labels[2]+" cluster : ", Counter(modelkmeans.labels_[14:20]).most_common(1)[0][0])

print("\n", test_judul[0], ":", predicted_labels_kmeans[0], \
"\n", test_judul[1], ":", predicted_labels_kmeans[1], \
"\n", test_judul[2], ":", predicted_labels_kmeans[2])

